package com.example.s1114743_s1115662_app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    List<PostModel> postList;

    private OnEventClickListener listener;

    public PostAdapter(Context context, List<PostModel> postList, OnEventClickListener listener) {
        this.postList = postList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public PostAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostAdapter.ViewHolder holder, int position) {

        holder.name.setText(postList.get(position).getName());
        holder.description.setText(postList.get(position).getDescription());
        holder.limit.setText(Integer.toString(postList.get(position).getLimit()));
        holder.date.setText(postList.get(position).getDate());
        holder.type.setText(postList.get(position).getType());
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name, description, limit, date, type;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.event_name);
            description = itemView.findViewById(R.id.event_description);
            limit = itemView.findViewById(R.id.event_people_limit);
            date = itemView.findViewById(R.id.event_date);
            type = itemView.findViewById(R.id.event_type);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view)  {
            try {
                listener.onClick(view, getAdapterPosition());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public interface OnEventClickListener {
        void onClick(View v, int position) throws JSONException;
    }

}
