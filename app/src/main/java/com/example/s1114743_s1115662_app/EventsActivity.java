package com.example.s1114743_s1115662_app;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;


public class EventsActivity extends AppCompatActivity {

    List<PostModel> postList = new ArrayList<>();

    String saveData;

    String EVENT_URL = "http://192.168.178.21:8000/api/auth/events";
    String EVENT_JOIN_URL = "http://192.168.178.21:8000/api/auth/join";

    PostAdapter adapter;

    RecyclerView recyclerView;

    private Button button;

    private PostAdapter.OnEventClickListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setOnClickListener();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        recyclerView=findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        Button toAddEventScreenButton = findViewById(R.id.toAddEventScreen);
        toAddEventScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toAddEventScreen();
            }
        });

        Button toJoinedEventsScreenButton = findViewById(R.id.toJoinedEventsScreen);
        toJoinedEventsScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toJoinedEventsScreen();
            }
        });

        GetData();
    }

    private void setOnClickListener() {
        listener = new PostAdapter.OnEventClickListener() {
            @Override
            public void onClick(View v, int position) throws JSONException {
                Log.d("EVENT", "onClick: " + postList.get(position).name);

//                JSONArray jsonArray = new JSONArray();
//                jsonArray.put(0, postList.get(position).name);
//
//                JoinEvent(jsonArray);

                JSONObject jsonBody = new JSONObject();
                jsonBody.put("name", postList.get(position).name);

                JoinEvent(jsonBody);
            }
        };
    }

    private void JoinEvent(JSONObject jsonObject) {
        Log.d("JOIN_EVENT", "JoinEvent: " + jsonObject);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest joinRequest = new JsonObjectRequest(Request.Method.POST, EVENT_JOIN_URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("JOIN_RESPONSE", "onResponse: " + response);

                try {

//                    EventModel eventModel = new EventModel(
//                            response.getJSONObject("event").getString("event_name"),
//                            response.getJSONObject("event").getString("description"),
//                            response.getJSONObject("event").getString("date"),
//                            response.getJSONObject("event").getString("type"),
//                            response.getJSONObject("event").getInt("people_limit"));

                    AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "production")
                            .allowMainThreadQueries()
                            .build();
                    db.eventDAO().insertAll(new EventModel (
                            response.getJSONObject("event").getString("event_name"),
                            response.getJSONObject("event").getString("description"),
                            response.getJSONObject("event").getString("date"),
                            response.getJSONObject("event").getString("type"),
                            response.getJSONObject("event").getInt("people_limit")
                    ));

                    toJoinedEventsScreen();

//                    AppDatabase db = AppDatabase.getInstance(getApplicationContext());
//                    new Thread(new InsertPostTask(db, postModel));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("POST_ERROR", "onErrorResponse: " + error);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                try {
                    FileInputStream fileInputStream = openFileInput("User.txt");
                    InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuffer stringBuffer = new StringBuffer();

                    String lines;
                    while ((lines = bufferedReader.readLine()) != null) {
                        stringBuffer.append(lines + "\n");
                    }

                    String token =  stringBuffer.toString();

                    Log.d("TOKEN", token);

                    final Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " + token);

                    return headers;

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return getHeaders();
            }
        };

        requestQueue.add(joinRequest);
    }

    private void GetData() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, EVENT_URL, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i <= response.length() - 1; i++) {

                    try {
                        JSONObject jsonObject = response.getJSONObject(i);
                        postList.add(new PostModel(
                                jsonObject.getString("event_name"),
                                jsonObject.getString("description"),
                                jsonObject.getString("date"),
                                jsonObject.getString("type"),
                                jsonObject.getInt("people_limit")
                        ));

                        saveData = postList.toString();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }

                    adapter = new PostAdapter(getApplicationContext(), postList, listener);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                    progressDialog.dismiss();

                }

                writeFile(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Toast.makeText(EventsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                EmptyRecyclerview();
            }
        });

        requestQueue.add(jsonArrayRequest);

    }

    public void EmptyRecyclerview() {

//        Hier data uit de room db halen en plaatsen in recyclerview (mogelijk)

        if (postList.isEmpty()) {
            Log.d("EMPTY", postList.toString());

            Intent toEventsScreenIntent = new Intent(this, JoinedEventsActivity.class);
            startActivity(toEventsScreenIntent);
            overridePendingTransition(R.anim.enter_right_to_left, R.anim.exit_right_to_left);
        }
        else {
            Log.d("FULL", postList.toString());
        }
    }

    public void writeFile(String data) {

        try {

            FileOutputStream fileOutputStream = openFileOutput("Events.txt", MODE_PRIVATE);
            fileOutputStream.write(data.getBytes());
            fileOutputStream.close();

            Toast.makeText(getApplicationContext(), "Events saved for offline use", Toast.LENGTH_LONG).show();

            readFile();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readFile() {

        try {
            FileInputStream fileInputStream = openFileInput("Events.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);

            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();

            String lines;
            while ((lines = bufferedReader.readLine()) != null) {
                stringBuffer.append(lines + "\n");
            }

            String eventData =  stringBuffer.toString();

//            Log.d("READ", eventData);
//            Toast.makeText(EventsActivity.this, stringBuffer.toString(), Toast.LENGTH_LONG).show();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void toAddEventScreen() {
        Intent toAddEventScreenIntent = new Intent(this, AddEventActivity.class);
        startActivity(toAddEventScreenIntent);
        overridePendingTransition(R.anim.enter_right_to_left, R.anim.exit_right_to_left);
    }

    public void toJoinedEventsScreen() {
        Intent toAddEventScreenIntent = new Intent(this, JoinedEventsActivity.class);
        startActivity(toAddEventScreenIntent);
        overridePendingTransition(R.anim.enter_right_to_left, R.anim.exit_right_to_left);
    }

}
