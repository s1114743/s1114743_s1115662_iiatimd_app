package com.example.s1114743_s1115662_app;

import android.util.Log;

public class GetEventTask implements Runnable {

    AppDatabase db;

    public GetEventTask(AppDatabase db) {
        this.db = db;
    }

    @Override
    public void run() {
        db.eventDAO().getAllEvents();

        Log.d("RUN", "run: "+ "GetEventTask run");
    }
}
