package com.example.s1114743_s1115662_app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import java.util.List;

public class JoinedEventsActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joined_events);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "production")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        List<EventModel> eventModels =  db.eventDAO().getAllEvents();

        mRecyclerView = findViewById(R.id.joined_recyclerview);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new EventAdapter(eventModels);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        final Button backToEventsScreenButton = findViewById(R.id.backToEventsScreen);
        backToEventsScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToEventsScreen();
            }
        });
    }

    public void backToEventsScreen() {
        Intent backToEventsScreenIntent = new Intent(this, EventsActivity.class);
        startActivity(backToEventsScreenIntent);
        overridePendingTransition(R.anim.enter_right_to_left, R.anim.exit_right_to_left);
    }
}
