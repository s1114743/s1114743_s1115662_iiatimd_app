package com.example.s1114743_s1115662_app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class AddEventActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstances) {
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_add_event);

        Button toEventsScreenButton = findViewById(R.id.addEvent);
        toEventsScreenButton.setOnClickListener(this);

        readFile();
    }

    @Override
    public void onClick(View v) {

        addEvent();

        Intent toEventsScreenIntent = new Intent(this, EventsActivity.class);
        startActivity(toEventsScreenIntent);
        overridePendingTransition(R.anim.enter_right_to_left, R.anim.exit_right_to_left);
    }

    public void readFile() {
        try {
            FileInputStream fileInputStream = openFileInput("User.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);

            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();

            String lines;
            while ((lines = bufferedReader.readLine()) != null) {
                stringBuffer.append(lines + "\n");
            }

            String token =  stringBuffer.toString();

            Log.d("TOKEN", token);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addEvent() {

        try {
            String URL = "http://192.168.178.21:8000/api/auth/create";
            JSONObject jsonBody = new JSONObject();


            EditText mEventName = findViewById(R.id.EventName);
            EditText mEventType = findViewById(R.id.EventType);
            EditText mEventDescription = findViewById(R.id.EventDescription);
            EditText mEventLimit = findViewById(R.id.EventLimit);
            EditText mEventDate = findViewById(R.id.EventDate);


            jsonBody.put("event_name", mEventName.getText().toString());
            jsonBody.put("type", mEventType.getText().toString());
            jsonBody.put("people_limit", mEventLimit.getText().toString());
            jsonBody.put("description", mEventDescription.getText().toString());
            jsonBody.put("date", mEventDate.getText().toString());

            JsonObjectRequest eventRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
//                    Toast.makeText(getApplicationContext(), "Response:  " + response.toString(), Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), "Evenement toegevoegd", Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("ERROR", error.toString());
                    onBackPressed();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    try {
                        FileInputStream fileInputStream = openFileInput("User.txt");
                        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);

                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        StringBuffer stringBuffer = new StringBuffer();

                        String lines;
                        while ((lines = bufferedReader.readLine()) != null) {
                            stringBuffer.append(lines + "\n");
                        }

                        String token =  stringBuffer.toString();

                        Log.d("TOKEN", token);

                        final Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization", "Bearer " + token);

                        return headers;

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return getHeaders();
                }
            };
            RequestQueue queue = VolleySingleton.getInstance(this.getApplicationContext()).getRequestQueue();
            queue.add(eventRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
