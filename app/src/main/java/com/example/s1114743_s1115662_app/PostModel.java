package com.example.s1114743_s1115662_app;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class PostModel {

    @PrimaryKey(autoGenerate = true)
    int id;

    @ColumnInfo
    String name;
    String description;
    String date;
    String type;

    @ColumnInfo
    int limit;

    public PostModel(String name, String description, String date, String type, int limit) {
        this.name = name;
        this.description = description;
        this.date = date;
        this.type = type;
        this.limit = limit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}

