package com.example.s1114743_s1115662_app;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface PostDAO {

    @Query("SELECT * FROM PostModel")
    LiveData<ArrayList<PostModel>> getAll();

    @Insert
    void InsertPost(PostModel postModel);

    @Delete
    void delete(PostModel postModel);
}
