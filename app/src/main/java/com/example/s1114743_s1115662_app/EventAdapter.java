package com.example.s1114743_s1115662_app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventViewHolder> {

    private List<EventModel> mEventList;

    public static class EventViewHolder extends RecyclerView.ViewHolder {

        public TextView event_name;
        public TextView event_description;
        public TextView event_people_limit;
        public TextView event_date;
        public TextView event_type;

        public EventViewHolder(@NonNull View itemView) {
            super(itemView);
            event_name = itemView.findViewById(R.id.event_name);
            event_description = itemView.findViewById(R.id.event_description);
            event_people_limit = itemView.findViewById(R.id.event_people_limit);
            event_date = itemView.findViewById(R.id.event_date);
            event_type = itemView.findViewById(R.id.event_type);
        }
    }

    public EventAdapter(List<EventModel> eventList) {
        mEventList = eventList;
    }

    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false);
        EventViewHolder evh = new EventViewHolder(v);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder holder, int position) {
        EventModel currentItem = mEventList.get(position);

        holder.event_name.setText(currentItem.getName());
        holder.event_description.setText(currentItem.getDescription());
        holder.event_people_limit.setText(Integer.toString(currentItem.getLimit()));
        holder.event_date.setText(currentItem.getDate());
        holder.event_type.setText(currentItem.getType());
    }

    @Override
    public int getItemCount() {
        return mEventList.size();
    }

}
