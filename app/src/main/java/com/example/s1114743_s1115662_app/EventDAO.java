package com.example.s1114743_s1115662_app;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface EventDAO {

    @Query("SELECT * FROM EventModel")
    List<EventModel> getAllEvents();

    @Insert
    void insertAll(EventModel... eventModels);

}
