package com.example.s1114743_s1115662_app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button toRegisterScreenButton = findViewById(R.id.toRegisterScreen);
        toRegisterScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toRegisterScreen();
            }
        });

        Button toLoginScreenButton = findViewById(R.id.toLoginScreen);
        toLoginScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toLoginScreen();
            }
        });

        readFile();
    }

    public void toRegisterScreen() {
        Intent toRegisterScreenIntent = new Intent(this, RegisterActivity.class);
        startActivity(toRegisterScreenIntent);
        overridePendingTransition(R.anim.enter_right_to_left, R.anim.exit_right_to_left);
    }

    public void toLoginScreen() {
        Intent toLoginScreenIntent = new Intent(this, LoginActivity.class);
        startActivity(toLoginScreenIntent);
        overridePendingTransition(R.anim.enter_right_to_left, R.anim.exit_right_to_left);
    }

    public void readFile() {

        try {
            FileInputStream fileInputStream = openFileInput("User.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);

            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();

            String lines;
            while ((lines = bufferedReader.readLine()) != null) {
                stringBuffer.append(lines + "\n");
            }

            String userData =  stringBuffer.toString();

            if (userData == null) {
                Log.d("userData", "Leeg");
            }

            Log.d("READ", userData);
//            Toast.makeText(EventsActivity.this, stringBuffer.toString(), Toast.LENGTH_LONG).show();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
