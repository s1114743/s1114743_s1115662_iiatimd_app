package com.example.s1114743_s1115662_app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;


public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button toLoginScreenButton = findViewById(R.id.registerButton);
        toLoginScreenButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent toLoginScreenIntent = new Intent(this, LoginActivity.class);
        startActivity(toLoginScreenIntent);
        overridePendingTransition(R.anim.enter_right_to_left, R.anim.exit_right_to_left);

        sendWorkPostRequest();
    }

    private void sendWorkPostRequest() {

        try {
            String URL = "http://192.168.178.21:8000/api/auth/signup";

            JSONObject jsonBody = new JSONObject();

            EditText mFirstName = findViewById(R.id.registerFirstName);
            EditText mLastName = findViewById(R.id.registerLastName);
            EditText mAge = findViewById(R.id.registerAge);
            EditText mEmail = findViewById(R.id.registerEmail);
            EditText mPassword = findViewById(R.id.registerPassword);
            EditText mPasswordConfirm = findViewById(R.id.registerPasswordConfirm);

            jsonBody.put("first_name", mFirstName.getText().toString());
            jsonBody.put("last_name", mLastName.getText().toString());
            jsonBody.put("age", mAge.getText().toString());
            jsonBody.put("email", mEmail.getText().toString());
            jsonBody.put("password", mPassword.getText().toString());
            jsonBody.put("password_confirmation", mPasswordConfirm.getText().toString());

            JsonObjectRequest registerRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

//                    Toast.makeText(getApplicationContext(), "Response:  " + response.toString(), Toast.LENGTH_SHORT).show();
                    Log.d("Token", response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    onBackPressed();

                }
            });
//            {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    final Map<String, String> headers = new HashMap<>();
//                    headers.put("Authorization", "Basic " + "c2FnYXJAa2FydHBheS5jb206cnMwM2UxQUp5RnQzNkQ5NDBxbjNmUDgzNVE3STAyNzI=");//put your token here
//                    return headers;
//                }
//            };
            RequestQueue queue = VolleySingleton.getInstance(this.getApplicationContext()).getRequestQueue();
            queue.add(registerRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), "Registered successfully", Toast.LENGTH_SHORT).show();

    }

}


