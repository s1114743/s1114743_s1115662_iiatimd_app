package com.example.s1114743_s1115662_app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstances) {
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_login);

        Button toEventsScreenButton = findViewById(R.id.loginButton);
        toEventsScreenButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent toEventsScreenIntent = new Intent(this, EventsActivity.class);
        startActivity(toEventsScreenIntent);
        overridePendingTransition(R.anim.enter_right_to_left, R.anim.exit_right_to_left);
        login();
    }

    private void login() {

        try {
            String URL = "http://192.168.178.21:8000/api/auth/login";
            JSONObject jsonBody = new JSONObject();
            EditText mEmail = findViewById(R.id.loginEmail);
            EditText mPassword = findViewById(R.id.loginPassword);

            jsonBody.put("email", mEmail.getText().toString());
            jsonBody.put("password", mPassword.getText().toString());

            JsonObjectRequest loginRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("login", response.toString());
                    try {
                        writeFile(response.getString("access_token"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    onBackPressed();
                }
            });
            RequestQueue queue = VolleySingleton.getInstance(this.getApplicationContext()).getRequestQueue();
            queue.add(loginRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), "Logged in successfully", Toast.LENGTH_SHORT).show();

    }

    public void writeFile(String data) {

        try {

            FileOutputStream fileOutputStream = openFileOutput("User.txt", MODE_PRIVATE);
            fileOutputStream.write(data.getBytes());
            fileOutputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
